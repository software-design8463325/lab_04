class TextDocument:
    def __init__(self, text=""):
        self.text = text

    def __str__(self):
        return self.text


class Memento:
    def __init__(self, state):
        self.state = state


class TextEditor:
    def __init__(self):
        self.document = TextDocument()
        self.history = []

    def write(self, text):
        self.document.text += text

    def save(self):
        self.history.append(Memento(self.document.text))

    def undo(self):
        if self.history:
            memento = self.history.pop()
            self.document.text = memento.state
        else:
            print("Немає станів для відновлення")

    def __str__(self):
        return str(self.document)


def main():
    editor = TextEditor()
    editor.write("Привіт, ")
    editor.save()
    print(editor)

    editor.write("світ!")
    editor.save()
    print(editor)

    editor.write(" Як справи?")
    print(editor)

    editor.undo()
    print(editor)

    editor.undo()
    print(editor)

if __name__ == "__main__":
    main()