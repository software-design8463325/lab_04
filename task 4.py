import urllib.request
import os

class ImageLoadingStrategy:
    def load(self, href):
        raise NotImplementedError("Підкласи повинні реалізувати цей метод")


class FileSystemStrategy(ImageLoadingStrategy):
    def load(self, href):
        if os.path.exists(href):
            with open(href, 'rb') as file:
                print("Зображення завантажено з файлової системи")
                return file.read()
        else:
            print("Файл не знайдено")
            return None


class NetworkStrategy(ImageLoadingStrategy):
    def load(self, href):
        try:
            with urllib.request.urlopen(href) as response:
                print("Зображення завантажено з мережі")
                return response.read()
        except Exception as e:
            print(f"Не вдалося завантажити зображення з мережі: {e}")
            return None


class Image:
    def __init__(self, href, strategy):
        self.href = href
        self.strategy = strategy

    def display(self):
        image_data = self.strategy.load(self.href)
        if image_data:
            print(f"Відображення зображення з {self.href}")
        else:
            print("Не вдалося відобразити зображення")


def main():
    file_image = Image("path/to/image.jpg", FileSystemStrategy())
    network_image = Image("http://example.com/image.jpg", NetworkStrategy())

    file_image.display()
    network_image.display()

if __name__ == "__main__":
    main()