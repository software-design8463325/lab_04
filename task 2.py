class CommandCentre:
    def __init__(self):
        self.aircrafts = []
        self.runways = []

    def request_permission_to_land(self, aircraft):
        for runway in self.runways:
            if runway.is_available():
                runway.allocate(aircraft)
                return True
        return False

    def add_aircraft(self, aircraft):
        self.aircrafts.append(aircraft)

    def add_runway(self, runway):
        self.runways.append(runway)


class Aircraft:
    def __init__(self, id, command_centre):
        self.id = id
        self.command_centre = command_centre
        self.command_centre.add_aircraft(self)

    def request_to_land(self):
        if self.command_centre.request_permission_to_land(self):
            print(f"Літаку {self.id} дозволено посадку.")
        else:
            print(f"Літаку {self.id} посадку не дозволено.")


class Runway:
    def __init__(self, id, command_centre):
        self.id = id
        self.command_centre = command_centre
        self.available = True
        self.command_centre.add_runway(self)

    def is_available(self):
        return self.available

    def allocate(self, aircraft):
        if self.available:
            self.available = False
            print(f"Злітно-посадкова смуга {self.id} виділена літаку {aircraft.id}")
        else:
            print(f"Злітно-посадкова смуга {self.id} наразі недоступна")


def main():
    command_centre = CommandCentre()
    runway1 = Runway(1, command_centre)
    runway2 = Runway(2, command_centre)
    aircraft1 = Aircraft(101, command_centre)
    aircraft2 = Aircraft(102, command_centre)

    aircraft1.request_to_land()
    aircraft2.request_to_land()

if __name__ == "__main__":
    main()