class EventListener:
    def __init__(self):
        self.events = {}

    def add_event_listener(self, event_type, callback):
        if event_type not in self.events:
            self.events[event_type] = []
        self.events[event_type].append(callback)

    def remove_event_listener(self, event_type, callback):
        if event_type in self.events:
            self.events[event_type].remove(callback)

    def dispatch_event(self, event_type):
        if event_type in self.events:
            for callback in self.events[event_type]:
                callback()


class LightHTMLElement:
    def __init__(self, tag_name):
        self.tag_name = tag_name
        self.event_listener = EventListener()

    def add_event_listener(self, event_type, callback):
        self.event_listener.add_event_listener(event_type, callback)

    def remove_event_listener(self, event_type, callback):
        self.event_listener.remove_event_listener(event_type, callback)

    def dispatch_event(self, event_type):
        self.event_listener.dispatch_event(event_type)


def click_handler():
    print("Елемент натиснутий!")

def mouseover_handler():
    print("Миша наведена на елемент!")


def main():
    div = LightHTMLElement("div")
    div.add_event_listener("click", click_handler)
    div.add_event_listener("mouseover", mouseover_handler)

    div.dispatch_event("click")
    div.dispatch_event("mouseover")

if __name__ == "__main__":
    main()