class SupportHandler:
    def __init__(self, level, next_handler=None):
        self.level = level
        self.next_handler = next_handler

    def handle_request(self):
        if self.resolve():
            print(f"Запит оброблено на рівні {self.level}")
        elif self.next_handler:
            self.next_handler.handle_request()
        else:
            print("Запит не вдалося обробити, повторюємо процес.")
            main()

    def resolve(self):
        raise NotImplementedError("Підкласи повинні реалізувати цей метод")


class LevelOneSupport(SupportHandler):
    def resolve(self):
        response = input("Ваше питання стосується рахунків? (так/ні): ")
        return response.lower() == "так"


class LevelTwoSupport(SupportHandler):
    def resolve(self):
        response = input("Ваше питання стосується технічної підтримки? (так/ні): ")
        return response.lower() == "так"


class LevelThreeSupport(SupportHandler):
    def resolve(self):
        response = input("Ваше питання стосується управління обліковим записом? (так/ні): ")
        return response.lower() == "так"


class LevelFourSupport(SupportHandler):
    def resolve(self):
        response = input("Ваше питання стосується відгуків? (так/ні): ")
        return response.lower() == "так"


def main():
    handler = LevelOneSupport(1, LevelTwoSupport(2, LevelThreeSupport(3, LevelFourSupport(4))))
    handler.handle_request()

if __name__ == "__main__":
    main()